**Instrucciones**:

    - Para instalar el proyecto se deberá hacer con composer  
    - Una vez instalado, se podrá correr, desde la raíz del proyecto, el comando 'php artisan bot:run' para ejecutar el bot

**Suposiciones**: 

    - Cada acción empieza valiendo $100 
    - El incremento/decremento del valor de una acción se genera al principio del programa para todos los días entre -10 y +10 
    - En la 2da estrategia, cuando se calcula el promedio de precios, por ej. suponiendo que tengo precios los días 1, 2, 3, 4, 5, 6 y pido doble del promedio 
    para el dia 5, para su cálculo, va a tener en cuenta los días 1, 2, 3 y 4, no el 5.
    - Se toman sólo días hábiles (lunes a viernes) sin tener en cuenta los feriados 
    
**Resultados**:
-
    Estrategia 1: 
        
        MONEY EARNED BY STRATEGY 1: -15.731  
        SHARES NOT SOLD BY STRATEGY: 9  
        MONEY EARNED AFTER SELLING REMAINING SHARES: -1.213  
        MONEY AT THE END: 998.787
-
    Estrategia 2:
     
        MONEY EARNED BY STRATEGY 2: -22.693
        SHARES NOT SOLD BY STRATEGY: 13
        MONEY EARNED AFTER SELLING REMAINING SHARES: -2.180
        MONEY AT THE END: 997.820 
    
**Walkthrough de la Corrida:**


Estrategia 1: 

    DAY: 1
    BUY: ALUA| Q = 10| P: 96
    BUY: BMA| Q = 10| P: 94
    BUY: CEPU| Q = 10| P: 99
    BUY: COME| Q = 10| P: 92
    BUY: GGAL| Q = 10| P: 94
    BUY: METR| Q = 10| P: 99
    BUY: TGSU2| Q = 10| P: 93
    BUY: TRAN| Q = 10| P: 95
    BUY: TXAR| Q = 10| P: 98
    BUY: VALO| Q = 10| P: 95
    MONEY AT THE END OF DAY: 990.450
    
    DAY: 2
    BUY: ALUA| Q = 10| P: 93
    BUY: BMA| Q = 11| P: 87
    BUY: TXAR| Q = 11| P: 90
    BUY: VALO| Q = 11| P: 87
    SELL: CEPU| Q = 10| P: 104
    SELL: COME| Q = 10| P: 102
    SELL: GGAL| Q = 10| P: 103
    SELL: METR| Q = 10| P: 105
    SELL: TGSU2| Q = 10| P: 102
    SELL: TRAN| Q = 10| P: 98
    MONEY AT THE END OF DAY: 992.756
    
    DAY: 3
    BUY: BMA| Q = 12| P: 78
    BUY: BYMA| Q = 9| P: 109
    BUY: COME| Q = 10| P: 98
    BUY: EDN| Q = 9| P: 111
    BUY: FRAN| Q = 9| P: 106
    BUY: METR| Q = 10| P: 97
    BUY: MIRG| Q = 9| P: 106
    BUY: SUPV| Q = 10| P: 99
    BUY: TGNO4| Q = 10| P: 96
    BUY: TGSU2| Q = 10| P: 93
    BUY: TS| Q = 9| P: 102
    BUY: VALO| Q = 12| P: 80
    SELL: ALUA| Q = 20| P: 101
    SELL: TXAR| Q = 21| P: 92
    MONEY AT THE END OF DAY: 985.176
    
    DAY: 4
    BUY: ALUA| Q = 10| P: 94
    BUY: BYMA| Q = 9| P: 102
    BUY: FRAN| Q = 10| P: 97
    BUY: GGAL| Q = 9| P: 104
    BUY: MIRG| Q = 9| P: 103
    BUY: PAMP| Q = 9| P: 111
    BUY: TS| Q = 10| P: 95
    BUY: TXAR| Q = 11| P: 86
    BUY: VALO| Q = 14| P: 71
    SELL: BMA| Q = 33| P: 82
    SELL: COME| Q = 10| P: 104
    SELL: SUPV| Q = 10| P: 105
    SELL: TGNO4| Q = 10| P: 99
    SELL: TGSU2| Q = 10| P: 103
    MONEY AT THE END OF DAY: 983.412
    
    DAY: 7
    BUY: ALUA| Q = 11| P: 90
    BUY: APBR| Q = 9| P: 108
    BUY: BMA| Q = 13| P: 74
    BUY: BYMA| Q = 10| P: 92
    BUY: CEPU| Q = 10| P: 99
    BUY: EDN| Q = 9| P: 110
    BUY: FRAN| Q = 10| P: 91
    BUY: GGAL| Q = 9| P: 102
    BUY: METR| Q = 11| P: 88
    BUY: MIRG| Q = 10| P: 96
    BUY: TGNO4| Q = 11| P: 90
    BUY: TGSU2| Q = 10| P: 99
    BUY: VALO| Q = 14| P: 69
    BUY: YPFD| Q = 9| P: 109
    SELL: TS| Q = 19| P: 105
    SELL: PAMP| Q = 9| P: 121
    SELL: TXAR| Q = 11| P: 95
    MONEY AT THE END OF DAY: 974.034
    
    DAY: 8
    BUY: ALUA| Q = 11| P: 86
    BUY: BMA| Q = 13| P: 72
    BUY: SUPV| Q = 9| P: 107
    BUY: TGSU2| Q = 10| P: 97
    BUY: TS| Q = 9| P: 101
    BUY: TXAR| Q = 11| P: 90
    BUY: VALO| Q = 15| P: 65
    BUY: YPFD| Q = 9| P: 101
    SELL: BYMA| Q = 28| P: 96
    SELL: FRAN| Q = 29| P: 96
    SELL: METR| Q = 21| P: 91
    SELL: MIRG| Q = 28| P: 103
    SELL: GGAL| Q = 18| P: 105
    SELL: APBR| Q = 9| P: 116
    SELL: CEPU| Q = 10| P: 103
    SELL: TGNO4| Q = 11| P: 97
    MONEY AT THE END OF DAY: 981.734
    
    DAY: 9
    BUY: APBR| Q = 9| P: 109
    BUY: BYMA| Q = 10| P: 91
    BUY: TGNO4| Q = 10| P: 96
    BUY: TS| Q = 10| P: 99
    BUY: TXAR| Q = 12| P: 82
    BUY: VALO| Q = 18| P: 55
    SELL: ALUA| Q = 32| P: 91
    SELL: BMA| Q = 26| P: 81
    SELL: TGSU2| Q = 20| P: 107
    SELL: YPFD| Q = 18| P: 104
    SELL: SUPV| Q = 9| P: 116
    MONEY AT THE END OF DAY: 985.993
    
    DAY: 10
    BUY: APBR| Q = 9| P: 101
    BUY: BYMA| Q = 12| P: 83
    BUY: COME| Q = 9| P: 102
    BUY: EDN| Q = 9| P: 106
    BUY: METR| Q = 10| P: 93
    BUY: SUPV| Q = 9| P: 106
    BUY: TGNO4| Q = 10| P: 93
    BUY: TGSU2| Q = 10| P: 100
    BUY: TRAN| Q = 9| P: 104
    BUY: TS| Q = 10| P: 95
    BUY: TXAR| Q = 12| P: 80
    SELL: VALO| Q = 94| P: 62
    MONEY AT THE END OF DAY: 981.384
    
    DAY: 11
    BUY: ALUA| Q = 10| P: 92
    BUY: BYMA| Q = 12| P: 78
    BUY: CEPU| Q = 9| P: 106
    BUY: CVH| Q = 8| P: 123
    BUY: EDN| Q = 10| P: 97
    BUY: GGAL| Q = 9| P: 106
    BUY: MIRG| Q = 8| P: 112
    BUY: TGNO4| Q = 11| P: 89
    BUY: VALO| Q = 17| P: 58
    BUY: YPFD| Q = 9| P: 103
    SELL: TS| Q = 29| P: 97
    SELL: TXAR| Q = 35| P: 87
    SELL: APBR| Q = 18| P: 110
    SELL: COME| Q = 9| P: 107
    SELL: METR| Q = 10| P: 103
    SELL: SUPV| Q = 9| P: 113
    SELL: TGSU2| Q = 10| P: 109
    SELL: TRAN| Q = 9| P: 113
    MONEY AT THE END OF DAY: 984.833
    
    DAY: 14
    BUY: ALUA| Q = 11| P: 86
    BUY: BMA| Q = 12| P: 81
    BUY: CVH| Q = 8| P: 120
    BUY: EDN| Q = 11| P: 89
    BUY: GGAL| Q = 10| P: 96
    BUY: METR| Q = 10| P: 97
    BUY: SUPV| Q = 9| P: 107
    BUY: TGSU2| Q = 9| P: 102
    BUY: TS| Q = 10| P: 92
    SELL: BYMA| Q = 34| P: 83
    SELL: TGNO4| Q = 31| P: 92
    SELL: CEPU| Q = 9| P: 111
    SELL: MIRG| Q = 8| P: 120
    SELL: VALO| Q = 17| P: 62
    SELL: YPFD| Q = 9| P: 111
    MONEY AT THE END OF DAY: 985.931
    
    DAY: 15
    BUY: ALUA| Q = 12| P: 81
    BUY: APBR| Q = 9| P: 108
    BUY: BYMA| Q = 12| P: 78
    BUY: EDN| Q = 12| P: 79
    BUY: PAMP| Q = 6| P: 151
    BUY: SUPV| Q = 9| P: 101
    BUY: TRAN| Q = 9| P: 104
    BUY: TS| Q = 10| P: 91
    BUY: TXAR| Q = 12| P: 81
    BUY: VALO| Q = 18| P: 53
    BUY: YPFD| Q = 9| P: 104
    SELL: GGAL| Q = 19| P: 103
    SELL: BMA| Q = 12| P: 85
    SELL: METR| Q = 10| P: 105
    MONEY AT THE END OF DAY: 979.607
    
    DAY: 16
    BUY: ALUA| Q = 12| P: 78
    BUY: APBR| Q = 10| P: 100
    BUY: BMA| Q = 12| P: 77
    BUY: CEPU| Q = 9| P: 107
    BUY: CVH| Q = 8| P: 114
    BUY: FRAN| Q = 9| P: 107
    BUY: GGAL| Q = 10| P: 96
    BUY: PAMP| Q = 6| P: 145
    BUY: SUPV| Q = 10| P: 95
    BUY: TXAR| Q = 13| P: 75
    BUY: YPFD| Q = 10| P: 97
    SELL: EDN| Q = 60| P: 82
    SELL: TS| Q = 20| P: 96
    SELL: BYMA| Q = 12| P: 87
    SELL: TRAN| Q = 9| P: 107
    SELL: VALO| Q = 18| P: 55
    MONEY AT THE END OF DAY: 979.021
    
    DAY: 17
    BUY: APBR| Q = 10| P: 92
    BUY: BMA| Q = 14| P: 67
    BUY: CVH| Q = 8| P: 112
    BUY: EDN| Q = 13| P: 75
    BUY: FRAN| Q = 10| P: 99
    BUY: MIRG| Q = 7| P: 130
    BUY: TGSU2| Q = 10| P: 100
    BUY: TS| Q = 10| P: 91
    SELL: ALUA| Q = 45| P: 86
    SELL: SUPV| Q = 28| P: 105
    SELL: TXAR| Q = 25| P: 83
    SELL: CEPU| Q = 9| P: 115
    SELL: GGAL| Q = 10| P: 103
    MONEY AT THE END OF DAY: 982.432
    
    DAY: 18
    BUY: APBR| Q = 12| P: 82
    BUY: BMA| Q = 15| P: 65
    BUY: CEPU| Q = 9| P: 106
    BUY: COME| Q = 8| P: 114
    BUY: FRAN| Q = 11| P: 89
    BUY: METR| Q = 8| P: 114
    BUY: SUPV| Q = 9| P: 103
    BUY: TGSU2| Q = 11| P: 90
    BUY: TS| Q = 12| P: 81
    BUY: TXAR| Q = 13| P: 73
    BUY: VALO| Q = 16| P: 61
    BUY: YPFD| Q = 10| P: 93
    SELL: CVH| Q = 32| P: 120
    SELL: PAMP| Q = 12| P: 152
    SELL: EDN| Q = 13| P: 82
    MONEY AT THE END OF DAY: 977.702
    
    DAY: 21
    BUY: BMA| Q = 15| P: 64
    BUY: COME| Q = 8| P: 112
    BUY: CVH| Q = 9| P: 110
    BUY: FRAN| Q = 12| P: 83
    BUY: SUPV| Q = 10| P: 96
    BUY: TS| Q = 12| P: 78
    BUY: TXAR| Q = 14| P: 67
    BUY: YPFD| Q = 12| P: 83
    SELL: TGSU2| Q = 30| P: 99
    SELL: APBR| Q = 41| P: 86
    SELL: MIRG| Q = 7| P: 139
    SELL: CEPU| Q = 9| P: 111
    SELL: METR| Q = 8| P: 123
    SELL: VALO| Q = 16| P: 68
    MONEY AT THE END OF DAY: 980.570
    
    DAY: 22
    BUY: ALUA| Q = 11| P: 88
    BUY: BYMA| Q = 10| P: 97
    BUY: CEPU| Q = 9| P: 102
    BUY: COME| Q = 9| P: 102
    BUY: EDN| Q = 12| P: 77
    BUY: GGAL| Q = 8| P: 112
    BUY: METR| Q = 8| P: 119
    BUY: MIRG| Q = 7| P: 135
    BUY: TGNO4| Q = 7| P: 130
    BUY: TGSU2| Q = 11| P: 90
    BUY: TRAN| Q = 8| P: 112
    BUY: TXAR| Q = 16| P: 59
    BUY: YPFD| Q = 13| P: 74
    SELL: BMA| Q = 56| P: 69
    SELL: FRAN| Q = 42| P: 86
    SELL: TS| Q = 34| P: 80
    SELL: SUPV| Q = 19| P: 105
    SELL: CVH| Q = 9| P: 120
    MONEY AT THE END OF DAY: 981.648
    
    DAY: 23
    BUY: ALUA| Q = 12| P: 83
    BUY: BMA| Q = 15| P: 65
    BUY: BYMA| Q = 10| P: 91
    BUY: COME| Q = 10| P: 100
    BUY: METR| Q = 8| P: 113
    BUY: SUPV| Q = 10| P: 99
    BUY: TGNO4| Q = 7| P: 128
    BUY: TGSU2| Q = 12| P: 80
    BUY: VALO| Q = 15| P: 66
    SELL: YPFD| Q = 54| P: 83
    SELL: TXAR| Q = 43| P: 69
    SELL: CEPU| Q = 9| P: 110
    SELL: EDN| Q = 12| P: 85
    SELL: GGAL| Q = 8| P: 116
    SELL: MIRG| Q = 7| P: 143
    SELL: TRAN| Q = 8| P: 119
    MONEY AT THE END OF DAY: 985.367
    
    DAY: 24
    BUY: ALUA| Q = 12| P: 78
    BUY: APBR| Q = 10| P: 92
    BUY: BMA| Q = 17| P: 57
    BUY: BYMA| Q = 11| P: 86
    BUY: CEPU| Q = 10| P: 100
    BUY: COME| Q = 10| P: 91
    BUY: EDN| Q = 12| P: 78
    BUY: GGAL| Q = 9| P: 111
    BUY: METR| Q = 9| P: 108
    BUY: MIRG| Q = 7| P: 133
    BUY: TGNO4| Q = 8| P: 121
    BUY: TRAN| Q = 8| P: 115
    BUY: TS| Q = 12| P: 82
    BUY: TXAR| Q = 15| P: 64
    BUY: VALO| Q = 15| P: 65
    SELL: TGSU2| Q = 23| P: 84
    SELL: SUPV| Q = 10| P: 102
    MONEY AT THE END OF DAY: 973.993
    
    DAY: 25
    BUY: ALUA| Q = 13| P: 74
    BUY: APBR| Q = 11| P: 85
    BUY: BMA| Q = 20| P: 49
    BUY: CEPU| Q = 10| P: 95
    BUY: COME| Q = 11| P: 85
    BUY: EDN| Q = 13| P: 76
    BUY: METR| Q = 9| P: 104
    BUY: SUPV| Q = 10| P: 93
    BUY: TRAN| Q = 8| P: 113
    BUY: VALO| Q = 17| P: 58
    SELL: BYMA| Q = 31| P: 95
    SELL: GGAL| Q = 9| P: 118
    SELL: TS| Q = 12| P: 92
    SELL: TXAR| Q = 15| P: 68
    MONEY AT THE END OF DAY: 970.618
    
    DAY: 28
    BUY: ALUA| Q = 15| P: 66
    BUY: BMA| Q = 23| P: 42
    BUY: BYMA| Q = 10| P: 92
    BUY: CEPU| Q = 11| P: 90
    BUY: EDN| Q = 15| P: 66
    BUY: GGAL| Q = 8| P: 113
    BUY: TGSU2| Q = 11| P: 85
    BUY: TRAN| Q = 9| P: 108
    BUY: TS| Q = 11| P: 89
    BUY: YPFD| Q = 12| P: 82
    SELL: COME| Q = 56| P: 92
    SELL: METR| Q = 34| P: 114
    SELL: TGNO4| Q = 22| P: 133
    SELL: VALO| Q = 47| P: 68
    SELL: APBR| Q = 21| P: 87
    SELL: MIRG| Q = 7| P: 137
    SELL: SUPV| Q = 10| P: 101
    MONEY AT THE END OF DAY: 979.934
    
    DAY: 29
    BUY: BMA| Q = 27| P: 37
    BUY: BYMA| Q = 11| P: 90
    BUY: COME| Q = 12| P: 83
    BUY: CVH| Q = 8| P: 122
    BUY: METR| Q = 9| P: 110
    BUY: PAMP| Q = 5| P: 187
    BUY: TGSU2| Q = 12| P: 77
    BUY: TRAN| Q = 10| P: 98
    BUY: TXAR| Q = 13| P: 75
    BUY: VALO| Q = 14| P: 67
    SELL: ALUA| Q = 63| P: 68
    SELL: CEPU| Q = 31| P: 93
    SELL: EDN| Q = 40| P: 72
    SELL: TS| Q = 11| P: 96
    SELL: YPFD| Q = 12| P: 89
    MONEY AT THE END OF DAY: 982.402
    
    DAY: 30
    BUY: ALUA| Q = 17| P: 58
    BUY: BMA| Q = 33| P: 30
    BUY: FRAN| Q = 9| P: 109
    BUY: MIRG| Q = 7| P: 140
    BUY: TGNO4| Q = 7| P: 139
    BUY: TRAN| Q = 10| P: 93
    BUY: TXAR| Q = 15| P: 66
    BUY: VALO| Q = 17| P: 57
    SELL: BYMA| Q = 21| P: 92
    SELL: TGSU2| Q = 23| P: 83
    SELL: COME| Q = 12| P: 85
    SELL: CVH| Q = 8| P: 127
    SELL: METR| Q = 9| P: 120
    SELL: PAMP| Q = 5| P: 197
    MONEY AT THE END OF DAY: 982.545
    
    DAY: 31
    BUY: APBR| Q = 11| P: 86
    BUY: BMA| Q = 38| P: 26
    BUY: BYMA| Q = 11| P: 90
    BUY: CEPU| Q = 10| P: 92
    BUY: SUPV| Q = 9| P: 104
    BUY: TGNO4| Q = 7| P: 131
    BUY: TGSU2| Q = 12| P: 77
    BUY: VALO| Q = 19| P: 51
    SELL: TRAN| Q = 45| P: 96
    SELL: TXAR| Q = 28| P: 69
    SELL: ALUA| Q = 17| P: 61
    SELL: FRAN| Q = 9| P: 113
    SELL: MIRG| Q = 7| P: 144
    MONEY AT THE END OF DAY: 984.269
    
    
Estrategia 2:

    DAY: 1
    BUY: ALUA| Q = 10| P: 96
    BUY: BMA| Q = 10| P: 94
    BUY: CEPU| Q = 10| P: 99
    BUY: COME| Q = 10| P: 92
    BUY: GGAL| Q = 10| P: 94
    BUY: METR| Q = 10| P: 99
    BUY: TGSU2| Q = 10| P: 93
    BUY: TRAN| Q = 10| P: 95
    BUY: TXAR| Q = 10| P: 98
    BUY: VALO| Q = 10| P: 95
    MONEY AT THE END OF DAY: 990.450
    
    DAY: 2
    BUY: ALUA| Q = 10| P: 93
    BUY: BMA| Q = 11| P: 87
    BUY: TXAR| Q = 11| P: 90
    BUY: VALO| Q = 11| P: 87
    MONEY AT THE END OF DAY: 986.616
    
    DAY: 3
    BUY: BMA| Q = 12| P: 78
    BUY: BYMA| Q = 9| P: 109
    BUY: COME| Q = 10| P: 98
    BUY: EDN| Q = 9| P: 111
    BUY: FRAN| Q = 9| P: 106
    BUY: METR| Q = 10| P: 97
    BUY: MIRG| Q = 9| P: 106
    BUY: SUPV| Q = 10| P: 99
    BUY: TGNO4| Q = 10| P: 96
    BUY: TGSU2| Q = 10| P: 93
    BUY: TS| Q = 9| P: 102
    BUY: VALO| Q = 12| P: 80
    MONEY AT THE END OF DAY: 975.084
    
    DAY: 4
    BUY: ALUA| Q = 10| P: 94
    BUY: BYMA| Q = 9| P: 102
    BUY: FRAN| Q = 10| P: 97
    BUY: GGAL| Q = 9| P: 104
    BUY: MIRG| Q = 9| P: 103
    BUY: PAMP| Q = 9| P: 111
    BUY: TS| Q = 10| P: 95
    BUY: TXAR| Q = 11| P: 86
    BUY: VALO| Q = 14| P: 71
    MONEY AT THE END OF DAY: 966.504
    
    DAY: 7
    BUY: ALUA| Q = 11| P: 90
    BUY: APBR| Q = 9| P: 108
    BUY: BMA| Q = 13| P: 74
    BUY: BYMA| Q = 10| P: 92
    BUY: CEPU| Q = 10| P: 99
    BUY: EDN| Q = 9| P: 110
    BUY: FRAN| Q = 10| P: 91
    BUY: GGAL| Q = 9| P: 102
    BUY: METR| Q = 11| P: 88
    BUY: MIRG| Q = 10| P: 96
    BUY: TGNO4| Q = 11| P: 90
    BUY: TGSU2| Q = 10| P: 99
    BUY: VALO| Q = 14| P: 69
    BUY: YPFD| Q = 9| P: 109
    MONEY AT THE END OF DAY: 952.997
    
    DAY: 8
    BUY: ALUA| Q = 11| P: 86
    BUY: BMA| Q = 13| P: 72
    BUY: SUPV| Q = 9| P: 107
    BUY: TGSU2| Q = 10| P: 97
    BUY: TS| Q = 9| P: 101
    BUY: TXAR| Q = 11| P: 90
    BUY: VALO| Q = 15| P: 65
    BUY: YPFD| Q = 9| P: 101
    MONEY AT THE END OF DAY: 945.399
    
    DAY: 9
    BUY: APBR| Q = 9| P: 109
    BUY: BYMA| Q = 10| P: 91
    BUY: TGNO4| Q = 10| P: 96
    BUY: TS| Q = 10| P: 99
    BUY: TXAR| Q = 12| P: 82
    BUY: VALO| Q = 18| P: 55
    SELL ALUA. Q = 52 AT PRICE: 91 | BUY DAY: 1
    SELL BMA. Q = 59 AT PRICE: 81 | BUY DAY: 1
    SELL CEPU. Q = 20 AT PRICE: 106 | BUY DAY: 1
    SELL COME. Q = 20 AT PRICE: 109 | BUY DAY: 1
    SELL GGAL. Q = 28 AT PRICE: 104 | BUY DAY: 1
    SELL METR. Q = 31 AT PRICE: 94 | BUY DAY: 1
    SELL TGSU2. Q = 40 AT PRICE: 107 | BUY DAY: 1
    SELL TRAN. Q = 10 AT PRICE: 114 | BUY DAY: 1
    SELL TXAR. Q = 55 AT PRICE: 82 | BUY DAY: 1
    SELL VALO. Q = 94 AT PRICE: 55 | BUY DAY: 1
    MONEY AT THE END OF DAY: 974.321
    
    DAY: 10
    BUY: APBR| Q = 9| P: 101
    BUY: BYMA| Q = 12| P: 83
    BUY: COME| Q = 9| P: 102
    BUY: EDN| Q = 9| P: 106
    BUY: METR| Q = 10| P: 93
    BUY: SUPV| Q = 9| P: 106
    BUY: TGNO4| Q = 10| P: 93
    BUY: TGSU2| Q = 10| P: 100
    BUY: TRAN| Q = 9| P: 104
    BUY: TS| Q = 10| P: 95
    BUY: TXAR| Q = 12| P: 80
    MONEY AT THE END OF DAY: 963.884
    
    DAY: 11
    BUY: ALUA| Q = 10| P: 92
    BUY: BYMA| Q = 12| P: 78
    BUY: CEPU| Q = 9| P: 106
    BUY: CVH| Q = 8| P: 123
    BUY: EDN| Q = 10| P: 97
    BUY: GGAL| Q = 9| P: 106
    BUY: MIRG| Q = 8| P: 112
    BUY: TGNO4| Q = 11| P: 89
    BUY: VALO| Q = 17| P: 58
    BUY: YPFD| Q = 9| P: 103
    SELL BYMA. Q = 62 AT PRICE: 78 | BUY DAY: 3
    SELL EDN. Q = 37 AT PRICE: 97 | BUY DAY: 3
    SELL FRAN. Q = 29 AT PRICE: 110 | BUY DAY: 3
    SELL MIRG. Q = 36 AT PRICE: 112 | BUY DAY: 3
    SELL SUPV. Q = 28 AT PRICE: 113 | BUY DAY: 3
    SELL TGNO4. Q = 52 AT PRICE: 89 | BUY DAY: 3
    SELL TS. Q = 48 AT PRICE: 97 | BUY DAY: 3
    MONEY AT THE END OF DAY: 982.473
    
    DAY: 14
    BUY: ALUA| Q = 11| P: 86
    BUY: BMA| Q = 12| P: 81
    BUY: CVH| Q = 8| P: 120
    BUY: EDN| Q = 11| P: 89
    BUY: GGAL| Q = 10| P: 96
    BUY: METR| Q = 10| P: 97
    BUY: SUPV| Q = 9| P: 107
    BUY: TGSU2| Q = 9| P: 102
    BUY: TS| Q = 10| P: 92
    SELL PAMP. Q = 9 AT PRICE: 153 | BUY DAY: 4
    MONEY AT THE END OF DAY: 975.262
    
    DAY: 15
    BUY: ALUA| Q = 12| P: 81
    BUY: APBR| Q = 9| P: 108
    BUY: BYMA| Q = 12| P: 78
    BUY: EDN| Q = 12| P: 79
    BUY: PAMP| Q = 6| P: 151
    BUY: SUPV| Q = 9| P: 101
    BUY: TRAN| Q = 9| P: 104
    BUY: TS| Q = 10| P: 91
    BUY: TXAR| Q = 12| P: 81
    BUY: VALO| Q = 18| P: 53
    BUY: YPFD| Q = 9| P: 104
    SELL APBR. Q = 36 AT PRICE: 108 | BUY DAY: 7
    SELL YPFD. Q = 36 AT PRICE: 104 | BUY DAY: 7
    MONEY AT THE END OF DAY: 972.543
    
    DAY: 16
    BUY: ALUA| Q = 12| P: 78
    BUY: APBR| Q = 10| P: 100
    BUY: BMA| Q = 12| P: 77
    BUY: CEPU| Q = 9| P: 107
    BUY: CVH| Q = 8| P: 114
    BUY: FRAN| Q = 9| P: 107
    BUY: GGAL| Q = 10| P: 96
    BUY: PAMP| Q = 6| P: 145
    BUY: SUPV| Q = 10| P: 95
    BUY: TXAR| Q = 13| P: 75
    BUY: YPFD| Q = 10| P: 97
    MONEY AT THE END OF DAY: 962.120
    
    DAY: 17
    BUY: APBR| Q = 10| P: 92
    BUY: BMA| Q = 14| P: 67
    BUY: CVH| Q = 8| P: 112
    BUY: EDN| Q = 13| P: 75
    BUY: FRAN| Q = 10| P: 99
    BUY: MIRG| Q = 7| P: 130
    BUY: TGSU2| Q = 10| P: 100
    BUY: TS| Q = 10| P: 91
    MONEY AT THE END OF DAY: 954.581
    
    DAY: 18
    BUY: APBR| Q = 12| P: 82
    BUY: BMA| Q = 15| P: 65
    BUY: CEPU| Q = 9| P: 106
    BUY: COME| Q = 8| P: 114
    BUY: FRAN| Q = 11| P: 89
    BUY: METR| Q = 8| P: 114
    BUY: SUPV| Q = 9| P: 103
    BUY: TGSU2| Q = 11| P: 90
    BUY: TS| Q = 12| P: 81
    BUY: TXAR| Q = 13| P: 73
    BUY: VALO| Q = 16| P: 61
    BUY: YPFD| Q = 10| P: 93
    SELL COME. Q = 17 AT PRICE: 114 | BUY DAY: 10
    SELL METR. Q = 28 AT PRICE: 114 | BUY DAY: 10
    SELL TGSU2. Q = 40 AT PRICE: 90 | BUY DAY: 10
    SELL TRAN. Q = 18 AT PRICE: 109 | BUY DAY: 10
    SELL TXAR. Q = 50 AT PRICE: 73 | BUY DAY: 10
    MONEY AT THE END OF DAY: 957.463
    
    DAY: 21
    BUY: BMA| Q = 15| P: 64
    BUY: COME| Q = 8| P: 112
    BUY: CVH| Q = 9| P: 110
    BUY: FRAN| Q = 12| P: 83
    BUY: SUPV| Q = 10| P: 96
    BUY: TS| Q = 12| P: 78
    BUY: TXAR| Q = 14| P: 67
    BUY: YPFD| Q = 12| P: 83
    SELL ALUA. Q = 45 AT PRICE: 91 | BUY DAY: 11
    SELL CEPU. Q = 27 AT PRICE: 111 | BUY DAY: 11
    SELL CVH. Q = 41 AT PRICE: 110 | BUY DAY: 11
    SELL GGAL. Q = 29 AT PRICE: 115 | BUY DAY: 11
    SELL VALO. Q = 51 AT PRICE: 68 | BUY DAY: 11
    MONEY AT THE END OF DAY: 968.196
    
    DAY: 22
    BUY: ALUA| Q = 11| P: 88
    BUY: BYMA| Q = 10| P: 97
    BUY: CEPU| Q = 9| P: 102
    BUY: COME| Q = 9| P: 102
    BUY: EDN| Q = 12| P: 77
    BUY: GGAL| Q = 8| P: 112
    BUY: METR| Q = 8| P: 119
    BUY: MIRG| Q = 7| P: 135
    BUY: TGNO4| Q = 7| P: 130
    BUY: TGSU2| Q = 11| P: 90
    BUY: TRAN| Q = 8| P: 112
    BUY: TXAR| Q = 16| P: 59
    BUY: YPFD| Q = 13| P: 74
    SELL BMA. Q = 68 AT PRICE: 69 | BUY DAY: 14
    SELL EDN. Q = 48 AT PRICE: 77 | BUY DAY: 14
    SELL SUPV. Q = 47 AT PRICE: 105 | BUY DAY: 14
    SELL TS. Q = 54 AT PRICE: 80 | BUY DAY: 14
    MONEY AT THE END OF DAY: 973.646
    
    DAY: 23
    BUY: ALUA| Q = 12| P: 83
    BUY: BMA| Q = 15| P: 65
    BUY: BYMA| Q = 10| P: 91
    BUY: COME| Q = 10| P: 100
    BUY: METR| Q = 8| P: 113
    BUY: SUPV| Q = 10| P: 99
    BUY: TGNO4| Q = 7| P: 128
    BUY: TGSU2| Q = 12| P: 80
    BUY: VALO| Q = 15| P: 66
    SELL BYMA. Q = 32 AT PRICE: 91 | BUY DAY: 15
    SELL PAMP. Q = 12 AT PRICE: 174 | BUY DAY: 15
    MONEY AT THE END OF DAY: 970.025
    
    DAY: 24
    BUY: ALUA| Q = 12| P: 78
    BUY: APBR| Q = 10| P: 92
    BUY: BMA| Q = 17| P: 57
    BUY: BYMA| Q = 11| P: 86
    BUY: CEPU| Q = 10| P: 100
    BUY: COME| Q = 10| P: 91
    BUY: EDN| Q = 12| P: 78
    BUY: GGAL| Q = 9| P: 111
    BUY: METR| Q = 9| P: 108
    BUY: MIRG| Q = 7| P: 133
    BUY: TGNO4| Q = 8| P: 121
    BUY: TRAN| Q = 8| P: 115
    BUY: TS| Q = 12| P: 82
    BUY: TXAR| Q = 15| P: 64
    BUY: VALO| Q = 15| P: 65
    SELL APBR. Q = 42 AT PRICE: 92 | BUY DAY: 16
    SELL FRAN. Q = 42 AT PRICE: 97 | BUY DAY: 16
    SELL YPFD. Q = 45 AT PRICE: 85 | BUY DAY: 16
    MONEY AT THE END OF DAY: 967.462
    
    DAY: 25
    BUY: ALUA| Q = 13| P: 74
    BUY: APBR| Q = 11| P: 85
    BUY: BMA| Q = 20| P: 49
    BUY: CEPU| Q = 10| P: 95
    BUY: COME| Q = 11| P: 85
    BUY: EDN| Q = 13| P: 76
    BUY: METR| Q = 9| P: 104
    BUY: SUPV| Q = 10| P: 93
    BUY: TRAN| Q = 8| P: 113
    BUY: VALO| Q = 17| P: 58
    SELL MIRG. Q = 21 AT PRICE: 134 | BUY DAY: 17
    MONEY AT THE END OF DAY: 960.770
    
    DAY: 28
    BUY: ALUA| Q = 15| P: 66
    BUY: BMA| Q = 23| P: 42
    BUY: BYMA| Q = 10| P: 92
    BUY: CEPU| Q = 11| P: 90
    BUY: EDN| Q = 15| P: 66
    BUY: GGAL| Q = 8| P: 113
    BUY: TGSU2| Q = 11| P: 85
    BUY: TRAN| Q = 9| P: 108
    BUY: TS| Q = 11| P: 89
    BUY: YPFD| Q = 12| P: 82
    MONEY AT THE END OF DAY: 951.140
    
    DAY: 29
    BUY: BMA| Q = 27| P: 37
    BUY: BYMA| Q = 11| P: 90
    BUY: COME| Q = 12| P: 83
    BUY: CVH| Q = 8| P: 122
    BUY: METR| Q = 9| P: 110
    BUY: PAMP| Q = 5| P: 187
    BUY: TGSU2| Q = 12| P: 77
    BUY: TRAN| Q = 10| P: 98
    BUY: TXAR| Q = 13| P: 75
    BUY: VALO| Q = 14| P: 67
    SELL COME. Q = 60 AT PRICE: 83 | BUY DAY: 21
    SELL TXAR. Q = 58 AT PRICE: 75 | BUY DAY: 21
    MONEY AT THE END OF DAY: 950.767
    
    DAY: 30
    BUY: ALUA| Q = 17| P: 58
    BUY: BMA| Q = 33| P: 30
    BUY: FRAN| Q = 9| P: 109
    BUY: MIRG| Q = 7| P: 140
    BUY: TGNO4| Q = 7| P: 139
    BUY: TRAN| Q = 10| P: 93
    BUY: TXAR| Q = 15| P: 66
    BUY: VALO| Q = 17| P: 57
    SELL ALUA. Q = 80 AT PRICE: 58 | BUY DAY: 22
    SELL CEPU. Q = 40 AT PRICE: 101 | BUY DAY: 22
    SELL GGAL. Q = 25 AT PRICE: 114 | BUY DAY: 22
    SELL METR. Q = 43 AT PRICE: 120 | BUY DAY: 22
    SELL TGNO4. Q = 29 AT PRICE: 139 | BUY DAY: 22
    SELL TGSU2. Q = 46 AT PRICE: 83 | BUY DAY: 22
    SELL TRAN. Q = 53 AT PRICE: 93 | BUY DAY: 22
    MONEY AT THE END OF DAY: 972.436
    
    DAY: 31
    BUY: APBR| Q = 11| P: 86
    BUY: BMA| Q = 38| P: 26
    BUY: BYMA| Q = 11| P: 90
    BUY: CEPU| Q = 10| P: 92
    BUY: SUPV| Q = 9| P: 104
    BUY: TGNO4| Q = 7| P: 131
    BUY: TGSU2| Q = 12| P: 77
    BUY: VALO| Q = 19| P: 51
    SELL BMA. Q = 173 AT PRICE: 26 | BUY DAY: 23
    SELL SUPV. Q = 29 AT PRICE: 104 | BUY DAY: 23
    SELL VALO. Q = 97 AT PRICE: 51 | BUY DAY: 23
    MONEY AT THE END OF DAY: 977.307