<?php


namespace Tests\Unit;

use Tests\TestCase;

class Trade extends TestCase
{
    public function testTradeHasPrice()
    {
        // Instantiate trade
        $trade = new \App\Classes\Trade();

        // Instantiate price
        $price = new \App\Classes\PriceDate();
        $price->setDate(date("d/m/Y"));
        $price->setPrice(20);

        $trade->setPriceDate($price);

        $this->assertEquals(20, $trade->getPrice());
    }

    public function testTradeHasShare()
    {
        // Instantiate trade
        $trade = new \App\Classes\Trade();

        // Instantiate a share
        $share = new \App\Classes\Share('ALUA');

        $trade->setShare($share);

        $this->assertEquals('ALUA', $trade->getSymbol());
    }
}