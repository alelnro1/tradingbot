<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class Strategy2 extends TestCase
{
    /**
     * Create share with prices
     */
    private function addPricesToShare()
    {
        $share = new \App\Classes\Share('TS');

        $price1 = new \App\Classes\PriceDate();
        $price1->setDate(2);
        $price1->setPrice(10);
        $share->addPrice($price1);

        $price2  = new \App\Classes\PriceDate();
        $price2->setDate(3);
        $price2->setPrice(9);
        $share->addPrice($price2);

        $price3  = new \App\Classes\PriceDate();
        $price3->setDate(4);
        $price3->setPrice(15);
        $share->addPrice($price3);

        $price4  = new \App\Classes\PriceDate();
        $price4->setDate(5);
        $price4->setPrice(14);
        $share->addPrice($price4);

        $price5  = new \App\Classes\PriceDate();
        $price5->setDate(6);
        $price5->setPrice(10);
        $share->addPrice($price5);

        $price6  = new \App\Classes\PriceDate();
        $price6->setDate(7);
        $price6->setPrice(10);
        $share->addPrice($price5);

        $price7  = new \App\Classes\PriceDate();
        $price7->setDate(8);
        $price7->setPrice(10);
        $share->addPrice($price5);

        return $share;
    }

    public function testAverageOfPricesTillToday()
    {
        $share = $this->addPricesToShare();

        // Average until day 6 = 10 + 9 + 15 + 14 = 48 / 4 = 12
        $average = $share->averagePriceUntilDay(6);

        $this->assertEquals(12, $average);
    }

    public function testAverageIsNotDouble()
    {
        $share = $this->addPricesToShare();

        $is_double = $share->priceIsDoubleOfAverageForDay(6);

        $this->assertFalse($is_double);
    }

    public function testAverageIsDouble()
    {
        $share = $this->addPricesToShare();

        $price1  = new \App\Classes\PriceDate();
        $price1->setDate(7);
        $price1->setPrice(5);
        $share->addPrice($price1);

        $price2  = new \App\Classes\PriceDate();
        $price2->setDate(8);
        $price2->setPrice(100);
        $share->addPrice($price2);

        $is_double = $share->priceIsDoubleOfAverageForDay(7);

        $this->assertTrue($is_double);
    }

    public function testShareBoughtMoreThan5DaysAgo()
    {
        $broker = new \App\Classes\Broker(1000000);

        $calendar = new \App\Classes\Calendar(01, 2019);

        $share = $this->addPricesToShare();

        $broker_share = $broker->buy($share, 10, 1);

        $more_than_5_days = $broker_share->shareBoughtMoreThan5DaysAgo(6, $calendar);

        $this->assertTrue($more_than_5_days);
    }
}