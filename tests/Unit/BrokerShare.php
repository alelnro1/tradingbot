<?php

namespace Tests\Unit;

use Tests\TestCase;

class BrokerShare extends TestCase
{
    public function testBrokerShareHasShare()
    {
        // Instantiate a share held by the broker
        $broker_share = new \App\Classes\BrokerShare();

        // Instantiate the share
        $share = new \App\Classes\Share('YPFD');

        $broker_share->setShare($share);

        $this->assertEquals($share, $broker_share->getShare());
    }

    public function testBrokerShareHasQuantity()
    {
        // Instantiate a share held by the broker
        $broker_share = new \App\Classes\BrokerShare();

        $broker_share->setQuantity(20);

        $this->assertEquals(20, $broker_share->getQuantity());
    }

    public function testBrokerHasBrokerShare()
    {
        // Instantiate a broker
        $broker = new \App\Classes\Broker(100);

        // Instantiate the share
        $share = new \App\Classes\Share('YPFD');

        // Instantiate a broker share
        $broker_share = new \App\Classes\BrokerShare();
        $broker_share->setShare($share);
        $broker_share->setQuantity(10);

        $broker->addBrokerShare($broker_share, 2);

        $this->assertEquals($broker_share, $broker->getBrokerShare($share->getSymbol()));
    }
}