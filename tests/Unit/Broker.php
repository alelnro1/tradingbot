<?php

namespace Tests\Unit;

use Tests\TestCase;

class Broker extends TestCase
{
    public function testBrokerHasMoney()
    {
        $broker = new \App\Classes\Broker(1000000);

        $this->assertEquals(1000000, $broker->getAvailableMoney());
    }

    public function testBrokerCanBuyShare()
    {
        // Instantiate a broker
        $broker = new \App\Classes\Broker(1000000);

        // Instantiate a price
        $price = new \App\Classes\PriceDate();
        $price->setPrice(10);
        $price->setDate(2);

        // Instantiate a share
        $share = new \App\Classes\Share('ALUA');
        $share->addPrice($price);

        $this->assertTrue($broker->canBuy($share, 5, 2));
    }

    public function testBrokerHasTrade()
    {
        // Instantiate a broker
        $broker = new \App\Classes\Broker(1000000);

        // Instantiate a trade
        $trade = new \App\Classes\Trade();

        // Create a price for the trade
        $price_date = new \App\Classes\PriceDate();
        $price_date->setPrice(96);
        $price_date->setDate(date('d/m/Y'));

        // Instantiate a share with the symbol
        $share = new \App\Classes\Share('GGAL');
        $share->addPrice($price_date);

        // Add share to trade
        $trade->setShare($share);
        $trade->setPriceDate($price_date);

        // Add trade to broker
        $broker->addTrade($trade);

        // Temporary trade for test unit
        $trades[] = $trade;

        $this->assertEquals($trades, $broker->getTrades());
    }

    public function testUpdateMoneyAvailable()
    {
        // Instantiate a broker
        $broker = new \App\Classes\Broker(1000000);

        $broker->updateMoneyAvailable(-100000);

        $this->assertEquals(900000, $broker->getAvailableMoney());
    }

    public function testBrokerBuyAmountOfShares()
    {
        // Instantiate a broker
        $broker = new \App\Classes\Broker(1000000);

        // Instantiate a price
        $price_date = new \App\Classes\PriceDate();
        $price_date->setDate(2);
        $price_date->setPrice(15.5);

        // Instantiate a share
        $share = new \App\Classes\Share('APBR');
        $share->addPrice($price_date);

        // Broker buys 5 shares
        $broker->buy($share, 5, 2);

        $broker_shares_ggal = $broker->getAmountShares($share);

        $this->assertEquals(5, $broker_shares_ggal);
    }

    public function testBrokerHasUpdatedMoneyAfterBuy()
    {
        // Instantiate a broker
        $broker = new \App\Classes\Broker(1000000);

        // Instantiate a price
        $price_date = new \App\Classes\PriceDate();
        $price_date->setDate(2);
        $price_date->setPrice(20);

        // Instantiate a share
        $share = new \App\Classes\Share('APBR');
        $share->addPrice($price_date);

        // Broker buys 5 shares
        $broker->buy($share, 5, 2);

        $this->assertEquals(999900, $broker->getAvailableMoney());
    }

    public function testBrokerCanNotBuyBecauseThereIsNoPriceForGivenDate()
    {
        // Instantiate a broker
        $broker = new \App\Classes\Broker(1000000);

        // Instantiate a price
        $price_date = new \App\Classes\PriceDate();
        $price_date->setDate(2);
        $price_date->setPrice(15.5);

        // Instantiate a share
        $share = new \App\Classes\Share('APBR');
        $share->addPrice($price_date);

        $buy_result = $share->dateHasPrice(4);

        $this->assertFalse($buy_result);
    }

    public function testBrokerRemoveShare()
    {
        // Instantiate a broker
        $broker = new \App\Classes\Broker(1000000);

        // Instantiate a price
        $price_date = new \App\Classes\PriceDate();
        $price_date->setDate(2);
        $price_date->setPrice(15.5);

        // Instantiate a share
        $share1 = new \App\Classes\Share('APBR');
        $share1->addPrice($price_date);

        // Instantiate another share
        $share2 = new \App\Classes\Share('GGAL');
        $share2->addPrice($price_date);

        $broker->buy($share1, 5, 2);
        $broker->buy($share2, 5, 2);

        $broker->removeShare($share1);

        $amount_shares = count($broker->getBrokerShares());

        $this->assertEquals(1, $amount_shares);
    }
}