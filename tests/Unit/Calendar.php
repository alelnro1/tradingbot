<?php
namespace Tests\Unit;

use Tests\TestCase;

class Calendar extends TestCase
{
    public function testGetWorkingDaysInMonth()
    {
        $calendar = new \App\Classes\Calendar(12, 2018);

        // We will get an array with the working dates of a month
        $dates = $calendar->getWorkingDays();

        // Verify that December has 21 working days (not counting holidays)
        $this->assertEquals(21, count($dates));
    }

    public function testGetLastWorkingDayOfMonth()
    {
        $calendar = new \App\Classes\Calendar(01, 2019);

        // We will get an array with the working dates of a month
        $last_working_day = $calendar->getLastWorkingDay();

        // Verify that 31 is the last working day (not counting holidays)
        $this->assertEquals(31, $last_working_day);
    }
}