<?php

namespace Tests\Unit;

use Tests\TestCase;

class Bot extends TestCase
{
    public function testBotHasShares()
    {
        // Instantiate a bot object
        $bot = new \App\Classes\Bot();

        // Instantiate a share object
        $share = new \App\Classes\Share('GGAL');

        $bot->addShare($share);

        $this->assertContains($share, $bot->getShares());
    }

    public function testBotHas21Shares()
    {
        // Instantiate a bot object
        $bot = new \App\Classes\Bot();

        // Instantiate broker
        $broker = new \App\Classes\Broker(1000000);
        $bot->setBroker($broker);

        // Symbols requested
        $symbols = [
            'ALUA', 'APBR', 'BMA', 'BYMA', 'CEPU', 'COME', 'CVH',
            'EDN', 'FRAN', 'GGAL', 'METR', 'MIRG', 'PAMP', 'SUPV',
            'TGNO4', 'TGSU2', 'TRAN', 'TS', 'TXAR', 'VALO', 'YPFD'
        ];

        /* Generate share objects and add prices for all working days */
        foreach ($symbols as $symbol) {
            $share = new \App\Classes\Share($symbol);

            // Add share for bot processing
            $bot->addShare($share);
        }

        $this->assertEquals(21, count($bot->getShares()));
    }

    public function testAllSharesHavePrices()
    {
        // Instantiate bot
        $bot = new \App\Classes\Bot();

        // Instantiate broker
        $broker = new \App\Classes\Broker(1000000);
        $bot->setBroker($broker);

        // Symbols requested
        $symbols = [
            'ALUA', 'APBR', 'BMA', 'BYMA', 'CEPU', 'COME', 'CVH',
            'EDN', 'FRAN', 'GGAL', 'METR', 'MIRG', 'PAMP', 'SUPV',
            'TGNO4', 'TGSU2', 'TRAN', 'TS', 'TXAR', 'VALO', 'YPFD'
        ];

        // Instantiate calendar for 01/2019
        $calendar = new \App\Classes\Calendar(1, 2019);

        // Get all working days
        $working_days = $calendar->getWorkingDays();

        /* Generate share objects and add prices for all working days */
        foreach ($symbols as $symbol) {
            $share = new \App\Classes\Share($symbol);

            // Add dynamic prices to shares
            foreach ($working_days as $key => $day) {

                // Instantiate a new price for specific date
                $price = new \App\Classes\PriceDate();
                $price->setDate($day);

                // Calculate price for this day based on previous price
                $random_price = $price->getCalculatedPrice();
                $price_day_before = $share->getPriceDayBefore($day, $calendar);

                $calc_price = $price_day_before->getPrice() + $random_price;

                $price->setPrice($calc_price);

                // Add price to share
                $share->addPrice($price);
            }

            // Add share for bot processing
            $bot->addShare($share);
        }

        $shares_have_prices = $this->sharesHavePrices($bot->getShares());

        $this->assertTrue($shares_have_prices);
    }

    /**
     * As 01/2019 has 23 working days => each share should have 23 prices
     *
     * @param $shares
     */
    private function sharesHavePrices($shares)
    {
        $prices = true;

        foreach ($shares as $share) {
            if (count($share->getPrices()) != 23) {
                $prices = false;
                break;
            }
        }

        return $prices;
    }
}