<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class Strategy1 extends TestCase
{
    public function testCotizacionDifiereEnMasde1Porc()
    {
        // Instancio una cotizacion
        $cotiz_dia1 = new \App\Classes\PriceDate;
        $cotiz_dia1->setDate(date("d/m/Y"));
        $cotiz_dia1->setPrice(10.0);

        // Instancio otra cotizacion
        $cotiz_dia2 = new \App\Classes\PriceDate;
        $cotiz_dia2->setDate(date("d/m/Y", strtotime("+1 day")));
        $cotiz_dia2->setPrice(10.1);

        $diferencia_porcentual = $cotiz_dia2->calculatePercDiferencePrice($cotiz_dia1);

        $this->assertGreaterThanOrEqual(1, $diferencia_porcentual);
    }

    public function testPriceIncreasedAtLeast2Perc()
    {
        // Instantiate a price
        $price_day1 = new \App\Classes\PriceDate;
        $price_day1->setDate(date("d/m/Y"));
        $price_day1->setPrice(10.0);

        // Instantiate another price
        $price_day2 = new \App\Classes\PriceDate;
        $price_day2->setDate(date("d/m/Y", strtotime("+1 day")));
        $price_day2->setPrice(12.0);

        $diferencia_porcentual = $price_day2->calculatePercDiferencePrice($price_day1);

        $this->assertGreaterThanOrEqual(2, $diferencia_porcentual);
    }

    public function testStrategyHasShares()
    {
        // Instantiate a bot object
        $bot = new \App\Classes\Bot();

        // Instantiate calendar
        $calendar = new \App\Classes\Calendar(12, 2018);

        // Set up strategy
        $strategy = new \App\Classes\Strategies\Strategy1($calendar);

        // Instantiate a share object
        $share = new \App\Classes\Share('GGAL');

        $bot->addShare($share);

        $this->assertContains($share, $bot->getShares());
    }

    public function testPriceDiffersInLessThan1Perc()
    {
        // Instancio una cotizacion
        $price_date1 = new \App\Classes\PriceDate;
        $price_date1->setDate(date("d/m/Y"));
        $price_date1->setPrice(10.0);

        // Instancio otra cotizacion
        $price_date2 = new \App\Classes\PriceDate;
        $price_date2->setDate(date("d/m/Y", strtotime("+1 day")));
        $price_date2->setPrice(10.05);

        $diferencia_porcentual = $price_date2->calculatePercDiferencePrice($price_date1);

        $this->assertLessThanOrEqual(1, $diferencia_porcentual);
    }

    public function testDecideWhereToBuyIfPriceFellAtLeast1Perc()
    {
        // Instantiate a bot
        $bot = new \App\Classes\Bot();

        // Instantiate a broker
        $broker = new \App\Classes\Broker(1000000);

        // Instantiate a share
        $share = new \App\Classes\Share('GGAL');

        // Instantiate calendar
        $calendar = new \App\Classes\Calendar(12, 2018);

        // Set up strategy
        $strategy = new \App\Classes\Strategies\Strategy1($calendar);

        // Get working days from calendar
        $working_days = $calendar->getWorkingDays();

        // Aux var for test
        $day_test = null;

        // Set up prices for share
        foreach ($working_days as $key => $day){
            // Instantiate a price
            $price_date = new \App\Classes\PriceDate();
            $price_date->setDate($day);

            if ($key == 0) {
                $price_date->setPrice(10.0);
            } else {
                $price_date->setPrice(9.9);

                $day_test = $day;
            }

            $share->addPrice($price_date);

            // 2 prices added => Exit foreach
            if ($key == 1)
                break;
        }

        // Add share to bot
        $bot->addShare($share);

        $shares_to_buy = $strategy->shouldBuy($day_test, $share, $broker);

        $this->assertGreaterThan(0, $shares_to_buy);
    }

    public function testDecideWhereToSellIfShareIncreasedValueAtLeast2Perc()
    {
        // Instantiate a bot
        $bot = new \App\Classes\Bot();

        // Instantiate a share
        $share_1 = new \App\Classes\Share('GGAL');

        // Instantiate calendar
        $calendar = new \App\Classes\Calendar(12, 2018);

        $price_day1 = new \App\Classes\PriceDate();
        $price_day1->setPrice(10);
        $price_day1->setDate(3);

        $price_day2 = new \App\Classes\PriceDate();
        $price_day2->setPrice(12);
        $price_day2->setDate(4);

        $share_1->addPrice($price_day1);
        $share_1->addPrice($price_day2);

        // Instantiate another share
        $share_2 = new \App\Classes\Share('ALUA');

        $shares[] = $share_1;

        // Instantiate a broker
        $broker = new \App\Classes\Broker(1000000);
        $broker->buy($share_1, 10, 3);

        $bot->addShare($share_1);

        $broker_share = $broker->getBrokerShare('GGAL');

        // Set up strategy
        $strategy = new \App\Classes\Strategies\Strategy1($calendar);

        $should_sell = $strategy->shouldSell(4, $broker_share, $broker);

        $this->assertTrue($should_sell);
    }

    public function testSellGivesCorrectReturn()
    {
        // Instantiate a bot
        $bot = new \App\Classes\Bot();

        // Instantiate a share
        $share = new \App\Classes\Share('GGAL');

        // Add prices to share
        $price_day1 = new \App\Classes\PriceDate();
        $price_day1->setPrice(10);
        $price_day1->setDate(2);

        $price_day2 = new \App\Classes\PriceDate();
        $price_day2->setPrice(12);
        $price_day2->setDate(3);

        $share->addPrice($price_day1);
        $share->addPrice($price_day2);

        // Instantiate a broker
        $broker = new \App\Classes\Broker(1000000);
        $broker->buy($share, 100, 2);

        // Now broker has $ 999.000

        foreach ($broker->getBrokerShares() as $key => $broker_share) {
            $broker->sell($broker_share, 3);
        }

        // Broker should have 999.000 + 100 * 12 = 1.000.200
        $this->assertEquals(1000200, $broker->getAvailableMoney());
    }
}
