<?php
namespace Tests\Unit;

use Tests\TestCase;

class Price extends TestCase
{
    public function testPriceIsFloat()
    {
        $cotizacion = new \App\Classes\PriceDate;

        $cotizacion->setPrice(290.25);

        $this->assertIsFloat($cotizacion->getPrice());
    }
}