<?php

namespace Tests\Unit;

use Tests\TestCase;

class Share extends TestCase
{

    public function testShareHasSymbol()
    {
        $this->assertClassHasAttribute('symbol', '\App\Classes\Share');
    }

    public function testShareHasPricesAttribute()
    {
        $this->assertClassHasAttribute('prices', '\App\Classes\Share');
    }

    public function testShareHasPrice()
    {
        // Instantiate a share
        $share = new \App\Classes\Share('GGAL');

        // Instantiate a new price
        $price1 = new \App\Classes\PriceDate;
        $price1->setPrice(100);
        $price1->setDate(date("d/m/Y"));

        // Instantiate a new price
        $price2 = new \App\Classes\PriceDate;
        $price2->setPrice(120);
        $price2->setDate(date("d/m/Y", strtotime("+1 day")));

        // Generate an array with prices
        $prices = [$price1, $price2];

        $share->setPrices($prices);

        $this->assertContains($price1, $prices);
    }

    public function testAddNewPriceToShare()
    {
        // Instantiate a share
        $share = new \App\Classes\Share('GGAL');

        // Instantiate a new price
        $price1 = new \App\Classes\PriceDate;
        $price1->setPrice(100);
        $price1->setDate(date("d/m/Y"));

        $share->addPrice($price1);

        $this->assertContains($price1, $share->getPrices());
    }

    public function testFindPriceByDay()
    {
        // Instantiate a share
        $share = new \App\Classes\Share('GGAL');

        // Tomorrow
        $tomorrow = date("d/m/Y", strtotime("+1 day"));

        // Instantiate a new price
        $price1 = new \App\Classes\PriceDate;
        $price1->setPrice(100);
        $price1->setDate(date("d/m/Y"));

        $share->addPrice($price1);

        // Instantiate a new price
        $price2 = new \App\Classes\PriceDate;
        $price2->setPrice(200);
        $price2->setDate($tomorrow);

        $share->addPrice($price2);

        // Instantiate a new price
        $price3 = new \App\Classes\PriceDate;
        $price3->setPrice(300);
        $price3->setDate(date("d/m/Y", strtotime("+2 days")));

        $share->addPrice($price3);

        $expected_price = $share->getPriceByDate($tomorrow);

        $this->assertEquals($expected_price, $expected_price);
    }

    public function testShareHasDynamicPrices()
    {
        // Instantiate a share
        $share = new \App\Classes\Share('GGAL');

        // Instantiate a calendar
        $calendar = new \App\Classes\Calendar(12, 2018);

        // Get working days of month
        $dates = $calendar->getWorkingDays();

        // Set prices array to validate test
        $prices = [];

        foreach ($dates as $key => $date) {
            // Instantiate a new price for specific date
            $price = new \App\Classes\PriceDate();
            $price->setDate($date);
            $price->setPrice($price->getCalculatedPrice());

            // Add price to share
            $share->addPrice($price);

            $prices[$price->getDate()] = $price;
        }

        // Aux var to check if price exists in share class
        $price_exists = true;

        $dynamic_prices = $share->getPrices();

        // Check that all prices had been added
        foreach ($dynamic_prices as $key => $dyn_price) {
            if (!isset($prices[$price->getDate()])) {
                $price_exists = false;
                break;
            }
        }

        $this->assertTrue($price_exists);
    }

    public function testGetPriceDayBefore()
    {
        $calendar = new \App\Classes\Calendar(12, 2018);
        $working_days = $calendar->getWorkingDays();

        $share = new \App\Classes\Share('GGAL');

        // Set up prices for share
        foreach ($working_days as $key => $day){
            // Instantiate a price
            $price_date = new \App\Classes\PriceDate();
            $price_date->setDate($day);

            if ($key == 0) {
                $price_date->setPrice(10.1);
            } else {
                $price_date->setPrice(9.9);

                $day_test = $day;
            }

            $share->addPrice($price_date);

            // 2 prices added => Exit foreach
            if ($key == 1)
                break;
        }

        $price_date = $share->getPriceDayBefore(4, $calendar);

        $this->assertEquals(10.1, $price_date->getPrice());
    }
}
