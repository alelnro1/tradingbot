<?php


namespace App\Classes;

class BrokerShare
{
    private $share;

    private $quantity;

    /**
     * Only first buying day is saved.
     * If the broker buys the same share in two different days, it will only save the first one.
     *
     * @var
     */
    private $buy_day;

    public function setShare(Share $share)
    {
        $this->share = $share;
    }

    public function getShare()
    {
        return $this->share;
    }

    public function getShareSymbol()
    {
        return $this->getShare()->getSymbol();
    }

    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }

    public function setBuyDay($day)
    {
        $this->buy_day = $day;
    }

    public function getBuyDay()
    {
        return $this->buy_day;
    }

    /**
     * Check if a share has been bought more than 5 days ago
     *
     * @param $actual_day
     * @param $working_days
     * @return bool
     */
    public function shareBoughtMoreThan5DaysAgo($actual_day, Calendar $calendar)
    {
        // Get working days of given calendar
        $working_days = $calendar->getWorkingDays();

        // Get buying day of share
        $buy_day = $this->getBuyDay();

        // Days that passed since the share has been bought
        $days_passed = 0;

        // Aux var to start counting days
        $count = false;

        foreach ($working_days as $working_day) {
            if ($working_day == $buy_day) {
                $count = true;
            }

            // Count!
            if ($count) {
                if ($working_day != $actual_day) {
                    $days_passed++;
                } else {
                    break;
                }
            }
        }

        // Share has been bought more than 5 days ago or not
        return $days_passed > 5;
    }
}