<?php

namespace App\Classes\Strategies;

use App\Classes\Broker;
use App\Classes\BrokerShare;
use Illuminate\Support\Facades\Log;

class Strategy2 extends Strategy implements IStrategy
{

    public function __construct($calendar)
    {
        $this->setCalendar($calendar);
    }

    /**
     * Run strategy each day
     *
     * @param $shares
     * @param $date
     * @param Broker $broker
     */
    public function run($shares, $date, Broker $broker)
    {
        Log::info("DAY: " . $date);

        foreach ($shares as $key => $share) {
            $quantity_to_buy = $this->shouldBuy($date, $share, $broker);

            // Buy only those shares that can be bought according to strategy
            if ($quantity_to_buy > 0) {
                $broker->buy($share, $quantity_to_buy, $date);

                $broker_share = $broker->getBrokerShare($share->getSymbol());

                Log::info("BUY: " . $share->getSymbol() . "| Q = " . $quantity_to_buy . "| P: " . $share->getPriceByDate($date)->getPrice() . "| Q in wallet: " . $broker_share->getQuantity());
            }
        }

        // After bought on this day, find shares of broker
        $shares_bought = $broker->getBrokerShares();

        // Sell those shares that can be sold according to strategy
        foreach ($shares_bought as $symbol => $broker_share) {
            if ($this->shouldSell($broker_share, $date, $broker)) {

                Log::info("SELL: " . $symbol . "| Q = " . $broker_share->getQuantity() . "| P: "  . $broker_share->getShare()->getPriceByDate($date)->getPrice());

                $broker->sell($broker_share, $date);
            }
        }
    }

    /**
     * Buy shares only if price fell at least 1% or if the price is the double of the average price
     *
     * @param $date
     * @param $shares
     * @param Broker $broker
     * @return int
     */
    public function shouldBuy($date, $share, Broker $broker)
    {
        // Iterate through each working day of the month and check whether should buy or not
        if (
            $share->priceFellAtLeast1Perc($date, $this->getCalendar())
            || $share->priceIsDoubleOfAverageForDay($date)
        ) {
            // Calculate quantity
            $quantity = $share->quantityToBuy($date);

            if ($broker->canBuy($share, $quantity, $date)) {
                return $quantity;
            }
        }

        return 0;
    }

    /**
     * Sell share if it has been bought more than 5 days ago
     *
     * @param $date
     * @param BrokerShare $broker_share
     * @return bool
     */
    private function shouldSell(BrokerShare $broker_share, $date, Broker $broker)
    {
        $share = $broker_share->getShare();

        if ($broker_share->shareBoughtMoreThan5DaysAgo($date, $this->getCalendar())) {

            // Calculate quantity
            $quantity = $broker_share->getQuantity();

            return $quantity > 0;
        }

        return false;
    }
}