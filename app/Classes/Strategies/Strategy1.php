<?php

namespace App\Classes\Strategies;

use App\Classes\Broker;
use App\Classes\BrokerShare;
use App\Classes\Calendar;
use App\Classes\Share;
use Illuminate\Support\Facades\Log;

class Strategy1 extends Strategy implements IStrategy
{
    public function __construct($calendar)
    {
        $this->setCalendar($calendar);
    }

    /**
     * Buy share only if price fell at least 1%
     *
     * @param $date
     * @param $shares
     * @param Broker $broker
     * @return bool
     */
    public function shouldBuy($date, Share $share, Broker $broker)
    {
        // Iterate through each working day of the month and check whether should buy or not
        if ($share->priceFellAtLeast1Perc($date, $this->getCalendar())) {

            // Calculate quantity
            $quantity = $share->quantityToBuy($date);

            if ($broker->canBuy($share, $quantity, $date)) {
                return $quantity;
            };
        }

        return 0;
    }

    /**
     * Share should be sold only if price got incremented at least 2%
     *
     * @param Share $share
     * @param $quantity
     */
    public function shouldSell($date, BrokerShare $broker_share, Broker $broker)
    {
        $share = $broker_share->getShare();

        /*Log::info("SHARE: " . $share->getSymbol());
        Log::info("PRICE DAY BEFORE: " . $share->getPriceDayBefore($date, $this->getCalendar())->getPrice());
        Log::info("PRICE TODAY: " . $share->getPriceByDate($date)->getPrice());
        Log::info("----");*/
        // Iterate through each working day of the month and check whether should buy or not
        $prev_price = $share->getPriceDayBefore($date, $this->getCalendar());
        $price_today = $share->getPriceByDate($date);

        $diference = $price_today->calculatePercDiferencePrice($prev_price);

        if ($diference >= 2) {
            // Calculate quantity
            $quantity = $broker->getAmountShares($share);

            return $quantity >= 0;
        }

        return false;
    }

    /**
     * Run strategy
     *
     * @param $shares
     * @param $date
     * @param Broker $broker
     */
    public function run($shares, $date, Broker $broker)
    {
        Log::info("DAY: " . $date);
        // Buy shares each date
        foreach ($shares as $key => $share) {
            $quantity_to_buy = $this->shouldBuy($date, $share, $broker);

            if ($quantity_to_buy > 0) {
                Log::info("BUY: " . $share->getSymbol() . "| Q = " . $quantity_to_buy . "| P: "  . $share->getPriceByDate($date)->getPrice());
                $broker->buy($share, $quantity_to_buy, $date);

                $broker_share = $broker->getBrokerShare($share->getSymbol());
                Log::info("SHARES IN WALLET: " . $broker_share->getQuantity());
            }
        }

        // After bought on this day, find shares of broker
        $shares_bought = $broker->getBrokerShares();

        // Sell those shares that can be sold according to strategy
        foreach ($shares_bought as $symbol => $broker_share) {

            if ($this->shouldSell($date, $broker_share, $broker)) {
                Log::info("SELL: " . $symbol . "| Q = " . $broker_share->getQuantity() . "| P: "  . $broker_share->getShare()->getPriceByDate($date)->getPrice());

                $broker->sell($broker_share, $date);
            }

        }
    }
}