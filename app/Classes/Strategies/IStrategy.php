<?php

namespace App\Classes\Strategies;

use App\Classes\Broker;
use App\Classes\Calendar;

interface IStrategy
{
    public function run($shares, $date, Broker $broker);
}