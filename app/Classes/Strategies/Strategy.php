<?php

namespace App\Classes\Strategies;

class Strategy
{
    private $calendar;

    public function setCalendar($calendar)
    {
        $this->calendar = $calendar;
    }

    public function getCalendar()
    {
        return $this->calendar;
    }
}