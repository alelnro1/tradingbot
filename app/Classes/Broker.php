<?php

namespace App\Classes;

use Illuminate\Support\Facades\Log;

class Broker
{
    private $available_money;

    private $broker_shares = [];

    private $trades = [];

    public function __construct($money)
    {
        $this->setMoneyavailable($money);
    }

    /**
     * @param $prices
     */
    public function setShares($shares)
    {
        $this->shares = $shares;
    }

    /**
     * @param $prices
     */
    public function setTrades($trades)
    {
        $this->trades = $trades;
    }

    public function setMoneyavailable($available_money)
    {
        $this->available_money = $available_money;
    }

    public function getAvailableMoney()
    {
        return $this->available_money;
    }

    /**
     * Broker only can buy if it has more than amount of shares * price of share
     *
     * @return bool
     */
    public function canBuy(Share $share, $amount, $date)
    {
        // Get share price
        $price_date = $share->getPriceByDate($date);

        // Calculate money needed
        $money_needed = $amount * $price_date->getPrice();

        return $this->getAvailableMoney() >= $money_needed;
    }

    /**
     * Remove a share from the broker
     *
     * @param Share $share
     * @param $amount
     */
    public function removeShare(Share $share)
    {
        unset($this->broker_shares[$share->getSymbol()]);
    }

    /**
     * Check if broker has symbol
     *
     * @param Share $share
     * @return mixed
     */
    public function hasShare(Share $share)
    {
        return isset($this->broker_shares[$share->getSymbol()]);
    }

    /**
     * Get all trades made by the broker
     *
     * @return array
     */
    public function getTrades()
    {
        return $this->trades;
    }

    /**
     * Add trade for broker history
     *
     * @param Trade $trade
     */
    public function addTrade(Trade $trade)
    {
        // Get broker trades
        $trades = $this->getTrades();

        // Create an aux array and add the new trade
        $trades[] = $trade;

        // Rewrite prices
        $this->setTrades($trades);
    }

    /**
     * Get amount of shares in wallet of a given share
     *
     * @param Share $share
     * @return mixed
     */
    public function getAmountShares(Share $share)
    {
        // Find broker share
        $broker_share = $this->getBrokerShare($share->getSymbol());

        return $broker_share->getQuantity();
    }

    /**
     * Update the money that the broker has available
     *
     * @param $change_of_money
     */
    public function updateMoneyAvailable($change_of_money)
    {
        // Get money available
        $money_available = $this->getAvailableMoney();

        // Add or substract amount of money
        $money_available += $change_of_money;

        $this->setMoneyavailable($money_available);
    }

    /**
     * Buy a share
     *
     * @param Share $share
     * @param $quantity
     * @param $date
     * @return Trade|bool
     */
    public function buy(Share $share, $quantity, $date)
    {
        // Get last price of share
        $price_date = $share->getPriceByDate($date);

        // Check if there is a price for given date
        if ($price_date) {
            // Calculate price to substract
            $price_substract = (-1) * $price_date->getPrice() * $quantity;

            // Substract money
            $this->updateMoneyAvailable($price_substract);

            // Find if broker helds share
            $broker_share = $this->getBrokerShare($share->getSymbol());

            // Create new broker share if non exists
            if ($broker_share) {
                $this->addQuantityBrokerShare($broker_share, $quantity);
            } else {
                $broker_share = new \App\Classes\BrokerShare();
                $broker_share->setQuantity($quantity);
                $broker_share->setShare($share);

                // Add share to broker
                $this->addBrokerShare($broker_share, $date);
            }

            // Save trade
            $trade = new \App\Classes\Trade();
            $trade->setShare($share);
            $trade->setPriceDate($price_date);
            $trade->setQuantity($quantity);

            $this->addTrade($trade);

            return $broker_share;
        } else {
            return false;
        }
    }

    /**
     * Sell a share
     *
     * @param Share $share
     * @param $quantity
     * @param $date
     * @return Trade|bool
     */
    public function sell(BrokerShare $broker_share, $date)
    {
        $share = $broker_share->getShare();

        // Get last price of share
        $price_date = $share->getPriceByDate($date);

        // Get quantity of shares of broker
        $quantity = $broker_share->getQuantity();

        // Check if there is a price for given date
        if ($price_date) {
            // Calculate price to add
            $money_add = $price_date->getPrice() * $quantity;

            // Substract money
            $this->updateMoneyAvailable($money_add);

            // Add shares to broker
            $this->removeShare($share);

            // Save trade
            $trade = new \App\Classes\Trade();
            $trade->setShare($share);
            $trade->setPriceDate($price_date);
            $trade->setQuantity((-1) * $quantity);

            $this->addTrade($trade);

            return $trade;
        } else {
            return false;
        }
    }

    /**
     * Add a share not held by broker
     *
     * @param BrokerShare $broker_share
     * @param $day
     */
    public function addBrokerShare(BrokerShare $broker_share, $day)
    {
        // Get broker shares
        $broker_shares = $this->getBrokerShares();

        // The held is not held by the broker => Save first buying day
        $broker_share->setBuyDay($day);

        $broker_shares[$broker_share->getShareSymbol()] = $broker_share;

        // Rewrite shares
        $this->setBrokerShares($broker_shares);
    }

    /**
     * Increment quantity of shares hold by broker
     *
     * @param BrokerShare $held_share
     * @param $quantity
     */
    public function addQuantityBrokerShare(BrokerShare $held_share, $quantity)
    {
        $held_share->setQuantity($quantity + $held_share->getQuantity());

        $this->broker_shares[$held_share->getShareSymbol()] = $held_share;
    }

    public function setBrokerShares($broker_shares)
    {
        $this->broker_shares = $broker_shares;
    }

    public function getBrokerShares()
    {
        return $this->broker_shares;
    }

    /**
     * Get broker share of a specific symbol
     *
     * @param $symbol
     * @return mixed|null
     */
    public function getBrokerShare($symbol)
    {
        $broker_shares = $this->getBrokerShares();

        if (isset($broker_shares[$symbol]))
            return $broker_shares[$symbol];

        return null;
    }
}