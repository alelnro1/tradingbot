<?php

namespace App\Classes;


class Trade
{
    private $price_date;

    private $share;

    private $quantity;

    public function setPriceDate(PriceDate $price_date)
    {
        $this->price_date = $price_date;
    }

    public function setShare(Share $share)
    {
        $this->share = $share;
    }

    public function getPrice()
    {
        return $this->price_date->getPrice();
    }

    public function getSymbol()
    {
        return $this->getShare()->getSymbol();
    }

    public function getShare()
    {
        return $this->share;
    }

    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }
}