<?php

namespace App\Classes;

use Illuminate\Support\Facades\Log;

class Bot
{
    private $shares = [];

    private $broker;

    public function getShares()
    {
        return $this->shares;
    }

    public function addShare(Share $share)
    {
        $this->shares[] = $share;
    }

    /**
     * @return mixed
     */
    public function getBroker()
    {
        return $this->broker;
    }

    /**
     * @param mixed $broker
     */
    public function setBroker($broker)
    {
        $this->broker = $broker;
    }

    public function run($strategy, Calendar $calendar)
    {
        // Get working days from specified month
        $dates = $calendar->getWorkingDays();

        $last_working_day = $calendar->getLastWorkingDay();

        // Apply strategy each working day for all shares available
        foreach ($dates as $key => $date) {
            //echo "DAY: " . $date . PHP_EOL;

            $strategy->run($this->getShares(), $date, $this->getBroker());

            //echo "MONEY AT THE END OF DAY: " . number_format($this->getBroker()->getAvailableMoney(), 0, ',', '.') . PHP_EOL . PHP_EOL;
        }

        Log::info("SHARES NOT SOLD BY STRATEGY: " . count($this->getBroker()->getBrokerShares()));

        // End of month => sell all other shares
        foreach ($this->getBroker()->getBrokerShares() as $symbol => $broker_share) {
            $this->getBroker()->sell($broker_share, $last_working_day);
        }

    }
}