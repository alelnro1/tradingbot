<?php

namespace App\Classes;


class PriceDate
{
    private $price;

    private $date;

    /**
     * @param $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * Calculamos la diferencia porcentual en el precio de una accion con respecto a
     * 2 días diferentes
     *
     * @param PriceDate $cotiz
     * @return float|int
     */
    public function calculatePercDiferencePrice(PriceDate $cotiz)
    {
        $diference = $this->getPrice() * 100 / $cotiz->getPrice() - 100;

        return $diference;
    }

    /**
     * Get dynamic calculated price (increase or decrease)
     *
     * @return float|int
     */
    public function getCalculatedPrice()
    {
        // Price will increase or decrease
        $multiplier = $this->getPriceMultiplier();

        // In how much will the price increase or decrease
        $number = rand(1, 10);

        return $multiplier * $number;
    }

    /**
     * Get price multiplier, defines if price increase or decrease
     *
     * @return int
     */
    private function getPriceMultiplier()
    {
        while (true) {
            $number = rand(-1, 1);

            if ($number !== 0) {
                return $number;
            }
        }
    }
}