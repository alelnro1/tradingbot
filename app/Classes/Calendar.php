<?php

namespace App\Classes;


class Calendar
{
    private $month;

    private $year;

    public function __construct($month, $year)
    {
        $this->setMonth($month);

        $this->setYear($year);
    }

    /**
     * @return mixed
     */
    public function getMonth()
    {
        return $this->month;
    }

    /**
     * @param mixed $month
     */
    private function setMonth($month)
    {
        $this->month = $month;
    }

    /**
     * @return mixed
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param mixed $year
     */
    private function setYear($year)
    {
        $this->year = $year;
    }

    /**
     * Get an array with working days in a month not taking in account holidays
     *
     * @return array
     */
    public function getWorkingDays()
    {
        try {
            $workdays = array();
            $month = $this->getMonth();
            $year = $this->getYear();
            $day_count = cal_days_in_month(CAL_GREGORIAN, $month, $year); // Get the amount of days

            // Loop through all days
            for ($i = 1; $i <= $day_count; $i++) {

                $date = $year . '/' . $month . '/' . $i; //format date
                $get_name = date('l', strtotime($date)); //get week day
                $day_name = substr($get_name, 0, 3); // Trim day name to 3 chars

                // If not a weekend add day to array
                if ($day_name != 'Sun' && $day_name != 'Sat') {
                    $workdays[] = $i;
                }
            }

            return $workdays;
        } catch (\Exception $e) {
            echo "ERROR: You must set month and year to ask for working days.";
        }
    }

    /**
     * Last working day
     *
     * @return mixed
     */
    public function getLastWorkingDay()
    {
        $working_days = $this->getWorkingDays();

        return last($working_days);
    }
}