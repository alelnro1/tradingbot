<?php

namespace App\Classes;

use SebastianBergmann\CodeCoverage\Report\PHP;

class Share
{
    private $symbol;

    private $prices = [];

    private $base_price = 100;


    /**
     * Each time a share is instantiated, it must have a symbol
     *
     * Share constructor.
     * @param $symbol
     */
    public function __construct($symbol)
    {
        $this->setSymbol($symbol);
    }

    /**
     * @param $prices
     */
    public function setPrices($prices)
    {
        $this->prices = $prices;
    }

    /**
     * @return mixed
     */
    public function getPrices()
    {
        return $this->prices;
    }

    /**
     * @return mixed
     */
    public function getSymbol()
    {
        return $this->symbol;
    }

    /**
     * @param mixed $symbol
     */
    public function setSymbol($symbol)
    {
        $this->symbol = $symbol;
    }

    public function addPrice(PriceDate $price)
    {
        // Get share prices
        $prices = $this->getPrices();

        // Create an aux array and add the new price
        $prices[$price->getDate()] = $price;

        // Rewrite prices
        $this->setPrices($prices);
    }

    /**
     * Check if a given date has a price
     *
     * @param $day
     * @return bool
     */
    public function dateHasPrice($day)
    {
        if ($day == 0)
            return false;

        return isset($this->getPrices()[$day]);
    }

    /**
     * Return price of day. If non set => return a base price
     *
     * @param $day
     * @return PriceDate
     */
    public function getPriceByDate($day)
    {
        if ($this->dateHasPrice($day)) {
            $price_date = $this->getPrices()[$day];
        } else {
            // No price in day before => set up base price
            $price_date = new \App\Classes\PriceDate();
            $price_date->setPrice($this->base_price);
        }

        return $price_date;
    }

    /**
     * Get price of the previous working day
     *
     * @param $day
     * @return PriceDate
     */
    public function getPriceDayBefore($day, $calendar)
    {
        $prev_price_date = $this->getPriceByDate(0);
        $prev_day = null;

        $working_days = $calendar->getWorkingDays();

        foreach ($working_days as $key => $working_day) {
            // Not in 1st iteration and day is requested
            if ($working_day == $day) {
                break;
            } else {
                /* First iteration of month, just save values */

                // Get share price for given date
                $price = $this->getPriceByDate($working_day);

                // Save aux price
                $prev_price_date = $price;
            }
        }

        return $prev_price_date;
    }

    /**
     * Calculate average price of share until a given date
     *
     * @param $day_requested
     * @return float|int
     */
    public function averagePriceUntilDay($day_requested)
    {
        // Initialize aux variable for storing total price until day requested
        $total = 0;

        // Amount days
        $amount_days = 0;

        foreach ($this->getPrices() as $day => $price_date) {

            // Day requested found => break
            if ($day_requested <= $day) {
                break;
            }

            // Add price for average
            $total += $price_date->getPrice();

            // Add amount of days
            $amount_days += 1;
        }

        // If at least one day passed, calculate average
        if ($amount_days > 0)
            return $total / $amount_days;

        return false;
    }

    /**
     *
     * @param $day_requested
     * @return bool
     */
    public function priceIsDoubleOfAverageForDay($day_requested)
    {
        $average = $this->averagePriceUntilDay($day_requested);

        // If the first day has been asked => no average
        if ($average) {
            // Get price today
            $price_today = $this->getPriceByDate($day_requested)->getPrice();

            return $average >= 2 * $price_today;
        }

        return false;
    }

    /**
     * Check if price fell at least 1% from one day to another
     *
     * @param $date
     * @return bool
     */
    public function priceFellAtLeast1Perc($date, $calendar)
    {
        $prev_price = $this->getPriceDayBefore($date, $calendar);
        $price_today = $this->getPriceByDate($date);

        $diference = $price_today->calculatePercDiferencePrice($prev_price);

        return $diference <= -1;
    }

    /**
     * Each time the broker decides to buy, it invests $1000. So calculate how many shares can it buy with that money
     *
     * @param $date
     * @return int
     */
    public function quantityToBuy($date)
    {
        $price_today = $this->getPriceByDate($date);

        $quantity = intval(1000 / $price_today->getPrice());

        return $quantity;
    }
}