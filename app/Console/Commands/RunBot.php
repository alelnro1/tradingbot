<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class RunBot extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bot:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run bot and find best strategy';

    private $shares = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Instantiate bot
        $bot = new \App\Classes\Bot();

        // Instantiate broker
        $broker = new \App\Classes\Broker(1000000);
        $bot->setBroker($broker);

        // Symbols requested
        $symbols = [
            'ALUA', 'APBR', 'BMA', 'BYMA', 'CEPU', 'COME', 'CVH',
            'EDN', 'FRAN', 'GGAL', 'METR', 'MIRG', 'PAMP', 'SUPV',
            'TGNO4', 'TGSU2', 'TRAN', 'TS', 'TXAR', 'VALO', 'YPFD'
        ];

        // Instantiate calendar for 01/2019
        $calendar = new \App\Classes\Calendar(1, 2019);

        // Get all working days
        $working_days = $calendar->getWorkingDays();

        // Last working day
        $last_working_day = $calendar->getLastWorkingDay();

        /* Generate share objects and add prices for all working days */
        foreach ($symbols as $symbol) {
            $share = new \App\Classes\Share($symbol);

            // Add dynamic prices to shares
            foreach ($working_days as $key => $day) {

                // Instantiate a new price for specific date
                $price = new \App\Classes\PriceDate();
                $price->setDate($day);

                // Calculate price for this day based on previous price
                $random_price = $price->getCalculatedPrice();
                $price_day_before = $share->getPriceDayBefore($day, $calendar);

                $calc_price = $price_day_before->getPrice() + $random_price;

                $price->setPrice($calc_price);

                // Add price to share
                $share->addPrice($price);
            }

            // Add share for bot processing
            $bot->addShare($share);
        }


        /* Start applying strategies */

        // Instantiate strategy 1
        $strategy1 = new \App\Classes\Strategies\Strategy1($calendar);

        // Instantiate strategy 2
        $strategy2 = new \App\Classes\Strategies\Strategy2($calendar);


        Log::info("-----------------------------------");
        Log::info("------------ STRATEGY 1 -----------");
        Log::info("-----------------------------------");

        // Results
        $bot->run($strategy1, $calendar);

        Log::info("MONEY AT THE END OF STRATEGY 1: " . number_format($broker->getAvailableMoney(), 0, ',', '.'));

        echo("-----------------------------------" . PHP_EOL);
        echo("------------ STRATEGY 1 -----------" . PHP_EOL);
        echo("-----------------------------------" . PHP_EOL);

        echo("MONEY AT THE END OF STRATEGY 1: " . number_format($broker->getAvailableMoney(), 0, ',', '.') . PHP_EOL);




        echo("-----------------------------------" . PHP_EOL);
        echo("------------ STRATEGY 2 -----------" . PHP_EOL);
        echo("-----------------------------------" . PHP_EOL);
        // Reset broker money
        $broker->setMoneyavailable(1000000);

        // Results
        $bot->run($strategy2, $calendar);

        Log::info("MONEY AT THE END OF STRATEGY 2: " . number_format($broker->getAvailableMoney(), 0, ',', '.'));

        echo("-----------------------------------" . PHP_EOL);
        echo("------------ STRATEGY 2 -----------" . PHP_EOL);
        echo("-----------------------------------" . PHP_EOL);

        echo("MONEY AT THE END OF STRATEGY 2: " . number_format($broker->getAvailableMoney(), 0, ',', '.') . PHP_EOL);

    }
}
